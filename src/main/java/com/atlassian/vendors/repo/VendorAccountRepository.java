package com.atlassian.vendors.repo;

import com.atlassian.vendors.domain.VendorAccount;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface VendorAccountRepository extends PagingAndSortingRepository<VendorAccount, Integer>
{
    VendorAccount findVendorAccountByCompanyName(String companyName);
}
