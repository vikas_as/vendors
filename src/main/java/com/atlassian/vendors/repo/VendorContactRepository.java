package com.atlassian.vendors.repo;

import java.util.List;

import com.atlassian.vendors.domain.VendorAccount;
import com.atlassian.vendors.domain.VendorContact;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface VendorContactRepository extends PagingAndSortingRepository<VendorContact, Integer>
{
    VendorContact findVendorContactByEmailAddress(String emailAddress);

    VendorContact findVendorContactByNameAndEmailAddress(String contactName, String emailAddress);

    List<VendorContact> findAllByVendorAccountId(Long vendorAccountId);
}
