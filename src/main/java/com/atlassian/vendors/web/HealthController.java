package com.atlassian.vendors.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController
{
    @GetMapping(path = "/health")
    public HttpStatus getStatus() {
        return HttpStatus.OK;
    }
}
