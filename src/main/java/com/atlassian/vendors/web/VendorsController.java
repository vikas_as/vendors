package com.atlassian.vendors.web;

import java.util.List;

import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.VendorAccountDto;
import com.atlassian.vendors.dto.VendorContactDto;
import com.atlassian.vendors.dto.VendorContactsListDto;
import com.atlassian.vendors.service.VendorAccountService;
import com.atlassian.vendors.service.VendorContactService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/vendors")
public class VendorsController
{
    private VendorAccountService vendorAccountService;
    private VendorContactService vendorContactService;

    @Autowired
    public VendorsController(VendorAccountService vendorAccountService, VendorContactService vendorContactService)
    {
        this.vendorAccountService = vendorAccountService;
        this.vendorContactService = vendorContactService;
    }

    @GetMapping(path = "/accounts")
    @ResponseBody
    public List<VendorAccountDto> getVendorAccounts(@RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "5") Integer pageSize,
                                                    @RequestParam(defaultValue = "accountId") String sortBy,
                                                    @RequestParam(defaultValue = "asc") String sortDir)
    {
        List<VendorAccountDto> list = vendorAccountService.getAllVendorsAccounts(pageNo, pageSize, sortBy, sortDir);
        return list;
    }

    @GetMapping(path = "/accounts/{accountName}")
    @ResponseBody
    public VendorAccountDto getVendorAccountByName(@PathVariable String accountName)
    {
        VendorAccountDto vendorAccountByName = vendorAccountService.getVendorAccountByName(accountName);
        return vendorAccountByName;
    }

    @PostMapping(path = "/accounts")
    @ResponseStatus(HttpStatus.CREATED)
    public String createVendorAccount(@RequestBody VendorAccountDto vendorAccountDto)
    {
        String createStatus = vendorAccountService.createVendorAccount(vendorAccountDto);
        return createStatus;
    }

    @PutMapping(path = "/accounts/{accountName}")
    @ResponseStatus(HttpStatus.OK)
    public String updateVendorAccount(@RequestBody VendorAccountDto vendorAccountDto, @PathVariable String accountName)
    {
        String updateStatus = vendorAccountService.updateVendorAccount(vendorAccountDto, accountName);
        return updateStatus;
    }

    @GetMapping(path = "/accounts/{accountName}/contacts")
    @ResponseStatus(HttpStatus.OK)
    public VendorAccountDto getVendorAccountContacts(@PathVariable String accountName) {
        return vendorAccountService.getVendorContactsByAccountName(accountName);
     }

    @PostMapping(path = "/accounts/{accountName}/contacts")
    @ResponseStatus(HttpStatus.CREATED)
    public String associateVendorAccountContacts(@RequestBody VendorContactsListDto vendorContactDtoList, @PathVariable String accountName) {
        return vendorAccountService.associateContactsToVendorAccounts(vendorContactDtoList.getVendorContactList(),accountName);
    }

    @GetMapping(path = "/contacts")
    @ResponseBody
    public List<VendorContactDto> getVendorContacts(@RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "5") Integer pageSize,
                                                    @RequestParam(defaultValue = "contactId") String sortBy,
                                                    @RequestParam(defaultValue = "asc") String sortDir)
    {
        List<VendorContactDto> list = vendorContactService.getAllVendorContacts(pageNo, pageSize, sortBy, sortDir);
        return list;
    }

    @GetMapping(path = "/contacts/{contactName}")
    @ResponseBody
    public VendorContactDto getVendorContactByName(@PathVariable String contactName, @RequestParam(required = true) String emailAddress)
    {
        VendorContactDto vendorContactDto = vendorContactService.getVendorContactByName(contactName, emailAddress);
        return vendorContactDto;
    }

    @PostMapping(path = "/contacts")
    @ResponseStatus(HttpStatus.CREATED)
    public String createVendorContact(@RequestBody VendorContactDto vendorContactDto)
    {
        String createStatus = vendorContactService.createVendorContact(vendorContactDto);
        return createStatus;
    }

    @PutMapping(path = "/contacts/{contactName}")
    @ResponseStatus(HttpStatus.OK)
    public String updateVendorContact(@RequestBody VendorContactDto vendorContactDto, @PathVariable String contactName)
    {
        String updateStatus = vendorContactService.updateVendorContact(vendorContactDto, contactName);
        return updateStatus;
    }
}
