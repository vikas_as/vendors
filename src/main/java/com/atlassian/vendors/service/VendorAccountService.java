package com.atlassian.vendors.service;

import java.util.List;

import com.atlassian.vendors.domain.VendorAccount;
import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.AssociateVendorContactDto;
import com.atlassian.vendors.dto.VendorAccountDto;
import com.atlassian.vendors.dto.VendorContactDto;

public interface VendorAccountService
{
    /**
     *
     * @return
     */
    List<VendorAccountDto> getAllVendorsAccounts(Integer pageNo, Integer pageSize, String sortBy, String sortDir);

    /**
     *
     * @param vendorName
     * @return
     */
    VendorAccountDto getVendorAccountByName(String vendorName);

    /**
     *
     * @param vendorAccountDto
     * @return
     */
    String createVendorAccount(VendorAccountDto vendorAccountDto);

    /**
     *
     * @param vendorAccountDto
     * @return
     */
    String updateVendorAccount(VendorAccountDto vendorAccountDto, String accountName);

    /**
     *
     * @param vendorName
     * @return
     */
    VendorAccountDto getVendorContactsByAccountName(String vendorName);

    /**
     *
     * @param vendorContacts
     * @param vendorName
     * @return
     */
    String associateContactsToVendorAccounts(List<AssociateVendorContactDto> vendorContacts, String vendorName);
}
