package com.atlassian.vendors.service;

import java.util.List;

import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.AssociateVendorContactDto;
import com.atlassian.vendors.dto.VendorContactDto;

public interface VendorContactService
{
    /**
     *
     * @return
     */
    List<VendorContactDto> getAllVendorContacts(Integer pageNo, Integer pageSize, String sortBy, String sortDir);

    /**
     *
     * @param name
     * @return
     */
    VendorContactDto getVendorContactByName(String name, String emailAddress);

    /**
     *
     * @param vendorContactDto
     * @return
     */
    String createVendorContact(VendorContactDto vendorContactDto);

    /**
     *
     * @param vendorContactDto
     * @return
     */
    String updateVendorContact(VendorContactDto vendorContactDto, String contactName);

    /**
     *
     * @param associateVendorContactDto
     * @return
     */
    VendorContact getVendorContactByName(AssociateVendorContactDto associateVendorContactDto);
}
