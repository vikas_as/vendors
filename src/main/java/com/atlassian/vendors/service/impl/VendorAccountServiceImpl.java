package com.atlassian.vendors.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.atlassian.vendors.domain.VendorAccount;
import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.AssociateVendorContactDto;
import com.atlassian.vendors.dto.VendorAccountDto;
import com.atlassian.vendors.dto.VendorContactDto;
import com.atlassian.vendors.exceptions.DuplicateVendorAccountException;
import com.atlassian.vendors.exceptions.VendorAccountNotFoundException;
import com.atlassian.vendors.repo.VendorAccountRepository;
import com.atlassian.vendors.service.VendorAccountService;
import com.atlassian.vendors.service.VendorContactService;
import com.atlassian.vendors.util.Vendors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class VendorAccountServiceImpl implements VendorAccountService
{
    private VendorAccountRepository vendorAccountRepository;

    private VendorContactService vendorContactService;

    @Autowired
    public VendorAccountServiceImpl(VendorAccountRepository vendorAccountRepository, VendorContactService vendorContactService)
    {
        this.vendorAccountRepository = vendorAccountRepository;
        this.vendorContactService = vendorContactService;
    }

    @Override
    public List<VendorAccountDto> getAllVendorsAccounts(Integer pageNo, Integer pageSize, String sortBy, String sortDir)
    {
        Sort sort;
        if (sortDir.equals("desc")) {
            sort = Sort.by(sortBy).descending();
        } else {
            sort = Sort.by(sortBy);
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, sort);
        Page<VendorAccount> pagedResult = vendorAccountRepository.findAll(paging);
        if (pagedResult.hasContent())
        {
            return pagedResult.getContent().stream().map(Vendors::toDto).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public VendorAccountDto getVendorAccountByName(String vendorName)
    {
        Optional<VendorAccount> vendorAccountOptional = Optional.ofNullable(vendorAccountRepository.findVendorAccountByCompanyName(vendorName));
        return vendorAccountOptional.map(Vendors::toDto).orElseThrow(() -> {
            throw new VendorAccountNotFoundException("Vendor account with name " + vendorName + " not found");
        });
    }

    @Override
    public String createVendorAccount(VendorAccountDto vendorAccountDto)
    {
        VendorAccount vendorAccount = Vendors.toEntity(vendorAccountDto);
        Optional.ofNullable(vendorAccountRepository.findVendorAccountByCompanyName(vendorAccount.getCompanyName())).ifPresentOrElse(va -> {
            throw new DuplicateVendorAccountException("Vendor account with company name " + vendorAccount.getCompanyName() + " already exists");
        }, () -> vendorAccountRepository.save(vendorAccount));
        return "Success";
    }

    @Override
    public String updateVendorAccount(VendorAccountDto vendorAccountDto, String accountName)
    {
        Optional.ofNullable(vendorAccountRepository.findVendorAccountByCompanyName(accountName)).ifPresentOrElse(vendorAccount -> {
            VendorAccount updatedAccount = Vendors.toEntity(vendorAccountDto);
            updatedAccount.setAccountId(vendorAccount.getAccountId());
            vendorAccountRepository.save(updatedAccount);
        }, () -> {
            throw new VendorAccountNotFoundException("Vendor account with name " + accountName + " does not exist. Please create it before trying to update");
        });
        return "Success";
    }

    @Override
    public VendorAccountDto getVendorContactsByAccountName(String vendorName)
    {
       VendorAccount vendorAccount = vendorAccountRepository.findVendorAccountByCompanyName(vendorName);
       VendorAccountDto vendorAccountDto = Vendors.toDto(vendorAccount);
       List<VendorContactDto> vendorContactDtoList = vendorAccount.getContacts().stream().map(Vendors::toDto).collect(Collectors.toList());
       vendorAccountDto.setContactList(vendorContactDtoList);
       return vendorAccountDto;
    }

    @Override
    public String associateContactsToVendorAccounts(List<AssociateVendorContactDto> vendorContacts, String accountName)
    {
        Optional.ofNullable(vendorAccountRepository.findVendorAccountByCompanyName(accountName)).ifPresentOrElse(vendorAccount -> {
            List<VendorContact> vendorContactList = vendorContacts.stream().map(vendorContactService::getVendorContactByName).collect(Collectors.toList());
            vendorAccount.getContacts().addAll(vendorContactList);
            vendorAccountRepository.save(vendorAccount);
        }, () -> {
            throw new VendorAccountNotFoundException("Vendor account with name " + accountName + " does not exist. Please create it before trying to update");
        });
        return "Success";
    }
}
