package com.atlassian.vendors.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.AssociateVendorContactDto;
import com.atlassian.vendors.dto.VendorContactDto;
import com.atlassian.vendors.exceptions.DuplicateVendorContactException;
import com.atlassian.vendors.exceptions.VendorContactNotFoundException;
import com.atlassian.vendors.repo.VendorContactRepository;
import com.atlassian.vendors.service.VendorContactService;
import com.atlassian.vendors.util.Vendors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class VendorContactServiceImpl implements VendorContactService
{
    private VendorContactRepository vendorContactRepository;

    @Autowired
    public VendorContactServiceImpl(VendorContactRepository vendorContactRepository)
    {
        this.vendorContactRepository = vendorContactRepository;
    }

    @Override
    public List<VendorContactDto> getAllVendorContacts(Integer pageNo, Integer pageSize, String sortBy, String sortDir)
    {
        Sort sort;
        if (sortDir.equals("desc")) {
            sort = Sort.by(sortBy).descending();
        } else {
            sort = Sort.by(sortBy);
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, sort);
        Page<VendorContact> pagedResult = vendorContactRepository.findAll(paging);
        if (pagedResult.hasContent())
        {
            return pagedResult.getContent().stream().map(Vendors::toDto).collect(Collectors.toList());
        }else {
            return new ArrayList<>();
        }
    }

    @Override
    public VendorContactDto getVendorContactByName(String contactName, String emailAddress)
    {
        Optional<VendorContact> vendorAccountOptional = Optional.ofNullable(vendorContactRepository.findVendorContactByNameAndEmailAddress(contactName, emailAddress));
        return vendorAccountOptional.map(Vendors::toDto).orElseThrow(() -> {
            throw new VendorContactNotFoundException("Vendor contact with name " + contactName + " not found");
        });
    }

    @Override
    public String createVendorContact(VendorContactDto vendorContactDto)
    {
        VendorContact vendorContact = Vendors.toEntity(vendorContactDto);
        Optional.ofNullable(vendorContactRepository.findVendorContactByEmailAddress(vendorContact.getEmailAddress())).ifPresentOrElse(vc -> {
            throw new DuplicateVendorContactException("Vendor contact with email address " + vendorContact.getEmailAddress() + " already exists");
        }, () -> vendorContactRepository.save(vendorContact));
        return "Success";
    }

    @Override
    public String updateVendorContact(VendorContactDto vendorContactDto, String contactName)
    {
        Optional.ofNullable(vendorContactRepository.findVendorContactByNameAndEmailAddress(contactName,vendorContactDto.getEmailAddress())).ifPresentOrElse(vendorContact -> {
            VendorContact updatedContact = Vendors.toEntity(vendorContactDto);
            updatedContact.setContactId(vendorContact.getContactId());
            updatedContact.setVendorAccountId(vendorContact.getVendorAccountId());
            vendorContactRepository.save(updatedContact);
        }, () -> {
            throw new VendorContactNotFoundException("Vendor contact with name " + contactName + " and email-address " + vendorContactDto.getEmailAddress() + " does not exist. Please create it before trying to update");
        });
        return "Success";
    }

    @Override
    public VendorContact getVendorContactByName(AssociateVendorContactDto associateVendorContactDto)
    {
        VendorContact vendorContact = vendorContactRepository.findVendorContactByNameAndEmailAddress(associateVendorContactDto.getName(), associateVendorContactDto.getEmailAddress());
        if (vendorContact == null)
        {
            throw new VendorContactNotFoundException("Vendor contact with name " + associateVendorContactDto.getName() + " not found");
        }
        return vendorContact;
    }
}
