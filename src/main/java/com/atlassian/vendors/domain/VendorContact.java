package com.atlassian.vendors.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@Table(name="VENDOR_CONTACT")
public class VendorContact
{
    public VendorContact(String name, String emailAddress, String addressLine1, String addressLine2, String city, String state, String postalCode, String country, Long vendorAccountId)
    {
        this.name = name;
        this.emailAddress = emailAddress;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.vendorAccountId = vendorAccountId;
    }

    @Id
    @SequenceGenerator(name = "VENDOR_CONTACT_SEQUENCE", sequenceName = "VENDOR_CONTACT_SEQUENCE_ID", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "VENDOR_CONTACT_SEQUENCE")
    @Column(name = "ID")
    private long contactId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "EMAIL_ADDRESS", unique = true, length = 100, nullable = false)
    private String emailAddress;

    @Column(name = "ADDRESS_LINE_1", length = 300, nullable = false)
    private String addressLine1;

    @Column(name = "ADDRESS_LINE_2", length=100, nullable = false)
    private String addressLine2;

    @Column(name = "CITY", nullable = false)
    private String city;

    @Column(name = "STATE", nullable = false)
    private String state;

    @Column(name = "POSTAL_CODE", length = 50, nullable = false)
    private String postalCode;

    @Column(name = "COUNTRY", length = 75, nullable = false)
    private String country;

    @Column(name = "VENDOR_ACCOUNT_ID", nullable = true)
    private Long vendorAccountId;

    public void setContactId(long contactId)
    {
        this.contactId = contactId;
    }

    public void setVendorAccountId(Long vendorAccountId) { this.vendorAccountId = vendorAccountId;}
}
