package com.atlassian.vendors.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "VENDOR_ACCOUNT")
public class VendorAccount
{
    public VendorAccount(String companyName, String addressLine1, String addressLine2, String city, String state, String postalCode, String country)
    {
        this.companyName = companyName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.contacts = new HashSet<>();
    }

    @Id
    @SequenceGenerator(name = "VENDOR_ACCOUNT_SEQUENCE", sequenceName = "VENDOR_ACCOUNT_SEQUENCE_ID", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "VENDOR_ACCOUNT_SEQUENCE")
    @Column(name = "ID")
    private long accountId;

    @Column(name = "COMPANY_NAME", unique = true, nullable = false)
    private String companyName;

    @Column(name = "ADDRESS_LINE_1", length = 300, nullable = false)
    private String addressLine1;

    @Column(name = "ADDRESS_LINE_2", length = 100, nullable = true)
    private String addressLine2;

    @Column(name = "CITY", nullable = false)
    private String city;

    @Column(name = "STATE", nullable = false)
    private String state;

    @Column(name = "POSTAL_CODE", length = 50, nullable = false)
    private String postalCode;

    @Column(name = "COUNTRY", length = 75, nullable = false)
    private String country;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "VENDOR_ACCOUNT_ID")
    private Set<VendorContact> contacts;

    public void setAccountId(long accountId)
    {
        this.accountId = accountId;
    }

    public void setContacts(Set<VendorContact> contacts)
    {
        this.contacts = contacts;
    }
}
