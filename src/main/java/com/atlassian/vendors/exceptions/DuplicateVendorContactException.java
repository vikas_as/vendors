package com.atlassian.vendors.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateVendorContactException extends RuntimeException
{
    public DuplicateVendorContactException(String message) {
       super(message);
    }
}
