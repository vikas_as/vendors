package com.atlassian.vendors.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateVendorAccountException extends RuntimeException
{
   public DuplicateVendorAccountException(String message) {
       super(message);
   }
}
