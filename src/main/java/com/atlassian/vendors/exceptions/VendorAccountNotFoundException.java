package com.atlassian.vendors.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class VendorAccountNotFoundException extends RuntimeException
{
    public VendorAccountNotFoundException(String message) {
        super( "Something" + message);
    }
}
