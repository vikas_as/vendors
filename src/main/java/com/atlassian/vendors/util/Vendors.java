package com.atlassian.vendors.util;

import com.atlassian.vendors.domain.VendorAccount;
import com.atlassian.vendors.domain.VendorContact;
import com.atlassian.vendors.dto.VendorAccountDto;
import com.atlassian.vendors.dto.VendorContactDto;

import org.springframework.beans.BeanUtils;

public class Vendors
{
    public static VendorAccountDto toDto(VendorAccount vendorAccount)
    {
        VendorAccountDto vendorAccountDto = new VendorAccountDto();
        BeanUtils.copyProperties(vendorAccount, vendorAccountDto);
        return vendorAccountDto;
    }

    public static VendorAccount toEntity(VendorAccountDto vendorAccountDto)
    {
        return new VendorAccount(vendorAccountDto.getCompanyName(), vendorAccountDto.getAddressLine1(),
            vendorAccountDto.getAddressLine2(), vendorAccountDto.getCity(),
            vendorAccountDto.getState(), vendorAccountDto.getPostalCode(),
            vendorAccountDto.getCountry());
    }

    public static VendorContactDto toDto(VendorContact vendorContact)
    {
        VendorContactDto vendorContactDto = new VendorContactDto();
        BeanUtils.copyProperties(vendorContact, vendorContactDto);
        return vendorContactDto;
    }

    public static VendorContact toEntity(VendorContactDto vendorContactDto)
    {
        return new VendorContact(vendorContactDto.getName(), vendorContactDto.getEmailAddress(),
            vendorContactDto.getAddressLine1(), vendorContactDto.getAddressLine2(),
            vendorContactDto.getCity(), vendorContactDto.getState(), vendorContactDto.getPostalCode(),
            vendorContactDto.getCountry(), null);
    }
}
