# Vendors Application

*    It is a spring boot application 
*    It uses in-memory h2 database (test data has been initialized in the DB)
*    Uses maven for dependency management
*    The application starts on port 8080 and uses embedded tomcat
*    The application has been configured with swagger which can be reached by http://localhost:8080/swagger-ui.html#/ 

## Running the application
Application can be run in multiple ways

#### Running in IDE
1. Import the project into your IDE and open the VendorsApplication.java
2. Run as Vendor Application
3. Once the application comes up access http://localhost:8080/swagger-ui.html#/ 

#### Running with maven and java
1.  Import/Clone the project 
2. If maven is installed run: **_mvn clean package_** or **_./mvnw clean package_** (if you don't have maven installed) which will create a jar file in your target folder
3. In your terminal or command line run : **_java -jar target/vendors-0.0.1-SNAPSHOT.jar_** 
4. Once the application comes up access http://localhost:8080/swagger-ui.html#/  

#### Running with maven spring-boot
1. Import/clone the project
2. If maven is installed run: **_mvn spring-boot:run_** or **_./mvnw spring-boot:run_** (if you don't have maven installed) 
3. Once the application comes up access http://localhost:8080/swagger-ui.html#/ 

#### Running as docker container (You will need docker installed and running for this)
1. Import/clone the project
2. If maven is installed run: **_mvn clean package_** or **_./mvnw clean package_** (if you don't have maven installed) which will create a jar file in your target folder
3. In the root folder of the project run: **_docker build -t {tag_name} ._**
4. Once the tag is built successfully run: **_docker run -p 8080:8080 {tag_name}_**
  
