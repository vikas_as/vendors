FROM openjdk:11
COPY /target/vendors-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "app.jar"]